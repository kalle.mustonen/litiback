package com.litiback.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    public static String HAIRIOT = "Kolmen auton kolari Sastamalassa, yksi loukkaantui vakavasti;Henkilöauto ajautunut kaiteeseen Kuopiossa moottoritiellä – kolaripaikalla odotettavissa ruuhkaa;Auto ajoi valotolppaa päin Villähteellä aamuliikenteessä;Liikenneonnettomuus Hämeenlinnanväylällä, liikenne takkuaa pohjoisen suuntaan;Onnettomuus tiellä 65 haittasi liikennettä Ylöjärvellä;Rekka lähti luisumaan mäessä Enontekiöllä – tie poikki tunteja;Kaksi liikenneonnettomuutta ykköstiellä Paimiossa – ei vakavia vahinkoja, liikenne sujuu taas;Onnettomuus hidasti aamuliikennettä ysitiellä Rauman pohjoispuolella;Onnettomuus hidasti aamuliikennettä kasitiellä Kokkolan pohjoispuolella;Onnettomuus hidasti aamuliikennettä ykköstiellä Kirkkonummen pohjoispuolella;Onnettomuus hidasti aamuliikennettä seiskatiellä Vaasan pohjoispuolella";

    public static String HAIRIO_JSON ="{\n" +
            "            \"attributes\": {\n" +
            "                \"SNMID\": 50373293,\n" +
            "                \"ERFID\": 50374135,\n" +
            "                \"ARE\": \"Veteli\",\n" +
            "                \"LDT\": \"Tie 18090 välillä Varesharju - Sulkaharju, Veteli.\\nTarkempi paikka: Paikasta Varesharju 1,7 km, suuntaan Pulkkinen. Konttikosken silta.\",\n" +
            "                \"MES\": \"Tie on suljettu liikenteeltä. \\n  Siltatyö\\n\\nKiertotie tien 751 kautta.\\n\",\n" +
            "                \"TIM\": \"Ajankohta: 13.08. klo 07:00 - 15.12.2020 klo 18:00.\",\n" +
            "                \"INP\": \"20200812172927\",\n" +
            "                \"TIT\": \"Tie 18090, Veteli. Liikennetiedote. \",\n" +
            "                \"DIV\": null,\n" +
            "                \"DOP\": \"RES\",\n" +
            "                \"CreationDate\": null\n" +
            "            },\n" +
            "            \"geometry\": {\n" +
            "                \"x\": 345172,\n" +
            "                \"y\": 7029032\n" +
            "            }\n" +
            "        \n" +
            "    \n" +
            "}\n";

    public static String HAIRIO_JSON1 ="{\n" +
            "            \"attributes\": {\n" +
            "                \"SNMID\": 50373293,\n" +
            "                \"ERFID\": 50374135,\n" +
            "                \"ARE\": \"Veteli\",\n" +
            "                \"LDT\": \"";

    public static String HAIRIO_JSON2 ="\",\n" +
            "                \"MES\": \"Tie on suljettu liikenteeltä. \\n  Siltatyö\\n\\nKiertotie tien 751 kautta.\\n\",\n" +
            "                \"TIM\": \"Ajankohta: 13.08. klo 07:00 - 15.12.2020 klo 18:00.\",\n" +
            "                \"INP\": \"20200812172927\",\n" +
            "                \"TIT\": \"Tie 18090, Veteli. Liikennetiedote. \",\n" +
            "                \"DIV\": null,\n" +
            "                \"DOP\": \"RES\",\n" +
            "                \"CreationDate\": null\n" +
            "            },\n" +
            "            \"geometry\": {\n" +
            "                \"x\": 345172,\n" +
            "                \"y\": 7029032\n" +
            "            }\n" +
            "        \n" +
            "    \n" +
            "}\n";


   @RequestMapping(value = "/resttest", method = RequestMethod.GET)
    public String resttest() {
        return "Hello world.";
    }

    @RequestMapping(value = "/number", method = RequestMethod.GET)
    public String number()
    {
        String s = "" + Math.random();
        s = s.substring(4,6);
        return s;
    }

    @RequestMapping(value = "/hairio", method = RequestMethod.GET)
    public String hairio()
    {
        String s = "" + Math.random();
        s = s.substring(4,5);
        Integer i = Integer.valueOf(s);
        String[] hairArray = HAIRIOT.split(";");
        return hairArray[i];
    }
    @RequestMapping(value = "/hairioJsonSta", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:3000")
    public String hairioJsonSta()
    {
        return HAIRIO_JSON;
    }

    @RequestMapping(value = "/hairioJson", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:3000")
    public String hairioJson()
    {
        return HAIRIO_JSON1 + hairio() + HAIRIO_JSON2;
    }


}